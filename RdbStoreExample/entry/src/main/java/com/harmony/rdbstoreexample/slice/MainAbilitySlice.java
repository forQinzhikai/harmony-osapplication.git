package com.harmony.rdbstoreexample.slice;
import com.harmony.rdbstoreexample.ResourceTable;
import com.harmony.rdbstoreexample.readSqliteFile;
import com.harmony.rdbstoreexample.sqliteData;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

import java.io.IOException;
import java.util.ArrayList;

import static ohos.agp.utils.TextAlignment.CENTER;

public class MainAbilitySlice extends AbilitySlice {
    private readSqliteFile readsqlite;
    String content;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        readsqlite = new readSqliteFile(this);
        try{
            readsqlite.init();
        }catch (IOException e){
            terminateAbility();
        }
        ArrayList<sqliteData> result = readsqlite.search();
        Text text = (Text)findComponentById(ResourceTable.Id_text_helloworld);
        text.setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
        text.setMultipleLine(true);
        text.setTextAlignment(CENTER);
        if(result.size() > 0){
            for(sqliteData data:result){
                text.insert(data.no + "  " + data.clubName + "\n", Text.AUTO_CURSOR_POSITION);
            }
        }
    }
    @Override
    public void onActive() {
        super.onActive();
    }
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
