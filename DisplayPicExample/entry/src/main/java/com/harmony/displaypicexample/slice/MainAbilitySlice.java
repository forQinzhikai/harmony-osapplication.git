package com.harmony.displaypicexample.slice;

import com.harmony.displaypicexample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.data.resultset.ResultSet;

import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;


public class MainAbilitySlice extends AbilitySlice {
    TableLayout img_layout;
    Text pre_text,text;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        String[] permission = {"ohos.permission.READ_USER_STORAGE"};
        requestPermissionsFromUser(permission,0);
        System.out.println("xxx:xxx");
        img_layout = (TableLayout)findComponentById(ResourceTable.Id_layout_id);
        img_layout.setColumnCount(3);
        pre_text = (Text)findComponentById(ResourceTable.Id_text_pre_id);
        text = (Text)findComponentById(ResourceTable.Id_text_id);
        displayPic();
    }
    public void displayPic(){
        img_layout.removeAllComponents();
        ArrayList<Integer> img_ids = new ArrayList<Integer>();
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        try {
            ResultSet result = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, null, null);
            if(result == null){
                pre_text.setVisibility(Component.VISIBLE);
            }else{
                pre_text.setVisibility(Component.HIDE);
            }
            while(result != null && result.goToNextRow()){
                int mediaId = result.getInt(result.getColumnIndexForName(AVStorage.Images.Media.ID));
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI,""+mediaId);
                FileDescriptor filedesc = helper.openFile(uri,"r");
                ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
                decodingOpts.desiredSize = new Size(300,300);
                ImageSource imageSource = ImageSource.create(filedesc,null);
                PixelMap pixelMap = imageSource.createThumbnailPixelmap(decodingOpts,true);
                Image img = new Image(MainAbilitySlice.this);
                img.setId(mediaId);
                img.setHeight(300);
                img.setWidth(300);
                img.setMarginTop(20);
                img.setMarginLeft(20);
                img.setPixelMap(pixelMap);
                img.setScaleMode(Image.ScaleMode.ZOOM_CENTER);
                img_layout.addComponent(img);
                System.out.println("xxx"+uri);
                img_ids.add(mediaId);
            }
        }catch (DataAbilityRemoteException | FileNotFoundException e){
            e.printStackTrace();
        }
        if(img_ids.size() > 0){
            pre_text.setVisibility(Component.HIDE);
            text.setVisibility(Component.VISIBLE);
            text.setText("照片数量："+img_ids.size());
        }else{
            pre_text.setVisibility(Component.VISIBLE);
            pre_text.setText("No picture.");
            text.setVisibility(Component.HIDE);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
        displayPic();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
