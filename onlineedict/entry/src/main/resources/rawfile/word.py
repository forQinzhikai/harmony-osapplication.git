import requests
import re 
words = {}
def add_word(word_line):
    global words 
    result1 = re.split('[a-z]+\.',word_line)
    pattern = re.compile('[a-z]+\.')
    result2 = pattern.findall(word_line)
    word = result1[0].strip()
    meanings = {}
    for i in range(0,len(result2)):
        key = result2[i].strip()
        value = result1[i + 1].strip()
        meanings[key] = value
    words[word] = meanings
r = requests.get('https://www.eol.cn/html/en/cetwords/cet4.shtml')
#print(r.content)
html = r.content
html_doc=str(html,'utf-8')
# print(html_doc)
from bs4 import BeautifulSoup
soup = BeautifulSoup(html_doc,'lxml')
# print(soup)
tags = soup.find_all(attrs = {'class':'wordL fl'})
# print(tags)
for tag in tags:
    p_list = tag.select('p')
    for p in p_list:
        print(p.text)
        add_word(p.text)
print(words)
print('单词抓取完毕')

import sqlite3
import os 

db_path = 'dict.sqlite'
if os.path.exists(db_path):
    os.remove(db_path)

conn = sqlite3.connect(db_path)
c = conn.cursor()
c.execute('''create table words (id int primary key not null, word varchar(30) not null,type varchar(10) not null,
meanings text not null);''')
c.execute('create index word_index on words(word)')
conn.commit()
conn.close()
print('数据创建成功')

conn = sqlite3.connect(db_path)
c = conn.cursor()
i = 1
for word in words:
    value = words[word]
    for type in value:
        meanings = value[type]
        sql = f'insert into words(id,word,type,meanings)\
            values({i},"{word}","{type}","{meanings}")';
        c.execute(sql)
        i += 1
    conn.commit()
conn.close()
print('电子词典数据库生成完毕')
